﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SE1616_Group1_A2.Models
{
    public partial class Country
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }
}
