﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SE1616_Group1_A2
{
    public partial class Form2 : Form
    {
        private static IConfiguration _configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json", true, true).Build();
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            string user = _configuration.GetConnectionString("User");
            string password = _configuration.GetConnectionString("Pass");
            if (textBox1.Text.Equals(user) && textBox2.Text.Equals(password))
            {
                MessageBox.Show("You are logged in as administrator!");
            }
            else
            {
                MessageBox.Show("Don't have that user!" + user + password);
            }
           
        }
    }
}
